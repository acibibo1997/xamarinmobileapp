﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EDREmake2
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        FirebaseHelper firebaseHelper = new FirebaseHelper();
        public MainPage()
        {
            InitializeComponent();
            picker1.Items.Add("Semi Synthetic");
            picker1.Items.Add("Fully Synthetic");
            

            picker2.Items.Add("Myvi");
            picker2.Items.Add("BMW 5 series");
            picker2.Items.Add("Volkswagen Scirocco");
            picker2.Items.Add("Kancil");
        }

        void OnReset(object sender, EventArgs e)
        {
            mileEntry.Text = null;
            avgKm.Text = null;
            picker1.SelectedIndex = -1;
            picker2.SelectedIndex = -1;
            error.Text = null;
        }

        void Onclick(object sender, EventArgs e)
        {

            var mEntry = int.Parse(mileEntry.Text);
            var avgEntry = int.Parse(avgKm.Text);
            var result1 = 0;
            var result2 = 0;
     
            


            if ((picker1.SelectedIndex == 0) && (picker2.SelectedIndex == 0))
            {
                result1 = 7000 + mEntry; 
                result2 = result1 / avgEntry;
                DateTime endDateTime = DateTime.Now.AddDays(result2);
     
                outputKm.Text = endDateTime.ToString("dd-MM-yyyy");
            }

            else if ((picker1.SelectedIndex == 1) && (picker2.SelectedIndex == 0))
            {
                result1 = 20000 + mEntry; 
                result2 = result1 / avgEntry;
                DateTime endDateTime = DateTime.Now.AddDays(result2);
                outputKm.Text = endDateTime.ToString("dd-MM-yyyy");
            }

            else if ((picker1.SelectedIndex == 0) && (picker2.SelectedIndex == 1))
            {
                result1 = 5000 + mEntry; 
                result2 = result1 / avgEntry;
                DateTime endDateTime = DateTime.Now.AddDays(result2);
                outputKm.Text = endDateTime.ToString("dd-MM-yyyy");
            }

            else if ((picker1.SelectedIndex == 1) && (picker2.SelectedIndex == 1))
            {
                result1 = 15000 + mEntry; 
                result2 = result1 / avgEntry;
                DateTime endDateTime = DateTime.Now.AddDays(result2);
                outputKm.Text = endDateTime.ToString("dd-MM-yyyy");
            }

            if ((picker1.SelectedIndex == 0) && (picker2.SelectedIndex == 2))
            {
                result1 = 5500 + mEntry; 
                result2 = result1 / avgEntry;
                DateTime endDateTime = DateTime.Now.AddDays(result2);

                outputKm.Text = endDateTime.ToString("dd-MM-yyyy");
            }

            if ((picker1.SelectedIndex == 1) && (picker2.SelectedIndex == 2))
            {
                result1 = 12000 + mEntry; 
                result2 = result1 / avgEntry;
                DateTime endDateTime = DateTime.Now.AddDays(result2);

                outputKm.Text = endDateTime.ToString("dd-MM-yyyy");
            }

            if ((picker1.SelectedIndex == 0) && (picker2.SelectedIndex == 3))
            {
                result1 = 8000 + mEntry; 
                result2 = result1 / avgEntry;
                DateTime endDateTime = DateTime.Now.AddDays(result2);

                outputKm.Text = endDateTime.ToString("dd-MM-yyyy");
            }

            if ((picker1.SelectedIndex == 1) && (picker2.SelectedIndex == 3))
            {
                result1 = 20000 + mEntry; 
                result2 = result1 / avgEntry;
                DateTime endDateTime = DateTime.Now.AddDays(result2);

                outputKm.Text = endDateTime.ToString("dd-MM-yyyy");
            }

            else if ((picker1.SelectedIndex == 0) && (picker2.SelectedIndex == -1))
            {
                outputKm.Text = null;
                error.Text = "Please Choose The Car Type";
            }

            else if ((picker1.SelectedIndex == -1) && (picker2.SelectedIndex == 0))
            {
                outputKm.Text = null;
                error.Text = "Please Choose The Oil Type";
            }

            else if ((picker1.SelectedIndex == 1) && (picker2.SelectedIndex == -1))
            {
                outputKm.Text = null;
                error.Text = "Please Choose The Car Type";
            }

            else if ((picker1.SelectedIndex == -1) && (picker2.SelectedIndex == 1))
            {
                outputKm.Text = null;
                error.Text = "Please Choose The Oil Type";
            }

            else if ((picker1.SelectedIndex==-1) && (picker2.SelectedIndex==2))
            {
                outputKm.Text = null;
                error.Text = "Please Choose The Oil Type";
            }
          
            else if ((picker1.SelectedIndex == -1) && (picker2.SelectedIndex == 3))
            {
                outputKm.Text = null;
                error.Text = "Please Choose The Oil Type";
            }


        }

        async void Saved(object sender, EventArgs e)
        {
            var pick1 = picker1.Items[picker1.SelectedIndex].ToString();
            var pick2 = picker2.Items[picker2.SelectedIndex].ToString();
            var mileage = mileEntry.Text;
            var avgkilometer = avgKm.Text;
            var date = outputKm.Text;
;           await firebaseHelper.AddRecord(pick1, pick2, mileage, avgkilometer, date);

            await DisplayAlert("Record Saved","Date to Change Engine Oil Has Been Saved", "Ok");
        }
    }
}
