﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EDREmake2
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SaveData : ContentPage

    {
        FirebaseHelper firebaseHelper = new FirebaseHelper();
        INotificationManager notificationManager;
        int notificationNumber = 0;


        protected async override void OnAppearing()
        {
            base.OnAppearing();
            displayRecord.ItemsSource = await firebaseHelper.GetAllsaveDataRec();
        }
        public SaveData()
        {
            InitializeComponent();
            notificationManager = DependencyService.Get<INotificationManager>();
            notificationManager.NotificationReceived += (sender, eventArgs) =>
            {
                var evtData = (NotificationEventArgs)eventArgs;
                ShowNotification(evtData.Title, evtData.Message);
            };

        }

        void OnScheduleClick(object sender, EventArgs e)
        {
            notificationNumber++;
            string title = $"EngineDoc #{notificationNumber}";
            string message = $"You have now received {notificationNumber} notifications! Don't forget to change your Engine Oil";
            notificationManager.ScheduleNotification(title, message);
        }

        void ShowNotification(string title, string message)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                var msg = new Label()
                {
                    Text = $"Notification Received:\nTitle: {title}\nMessage: {message}"
                };
                //stacklayout.Children.Add(msg);
            });
        }



    }
    

}