﻿using System;
using System.Collections.Generic;
using System.Text;
using Firebase;
using Firebase.Database;
using Firebase.Database.Query;
using System.Linq;
using System.Threading.Tasks;

namespace EDREmake2
{
    class FirebaseHelper
    {
        FirebaseClient firebase = new FirebaseClient("https://enginedoc-a68df.firebaseio.com/");
        public async Task AddRecord(string p1, string p2, string mE, string avgK, string date)
        {
            await firebase
                .Child("saveDataRec")
                .PostAsync(new saveDataRec() { picker1 = p1, picker2 = p2, mileEntry = mE, avgKm = avgK , ouputKM=date});

        }
        public async Task<List<saveDataRec>> GetAllsaveDataRec()
        {
            return (await firebase
                .Child("saveDataRec")
                .OnceAsync<saveDataRec>()).Select(item => new saveDataRec
                {
                    picker1 = item.Object.picker1,
                    picker2 = item.Object.picker2,
                    mileEntry = item.Object.mileEntry,
                    avgKm = item.Object.avgKm,
                    ouputKM = item.Object.ouputKM,
                   
                }).ToList();
        }

    
    }

    

    
}
